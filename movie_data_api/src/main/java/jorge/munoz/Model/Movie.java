package jorge.munoz.Model;

public class Movie {
    private int id;
    private String title;
    private int year;
    private String poster;
    private String info;
    private String director;

    public Movie() {
    }

    public Movie(int id, String title, int year, String poster, String info, String director) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.poster = poster;
        this.info = info;
        this.director = director;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

}
