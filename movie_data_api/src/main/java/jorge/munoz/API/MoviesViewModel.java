package jorge.munoz.API;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MoviesViewModel {

    @GetMapping("/movie/detail/{id}")
    public ModelAndView loadDetail(@PathVariable("id") int id){
        ModelAndView mv = new ModelAndView("detailMovie");
        mv.addObject("movie", MoviesController.getMovieDetailbyID(id));
        return mv;
    }

    @GetMapping("/movie/search")
    public ModelAndView listMovies(@RequestParam(name = "title", required = false) String title,
    @RequestParam(name = "year", required = false) Integer year){
        ModelAndView mv = new ModelAndView("searchTH");
        mv.addObject("movie", MoviesController.findMovieFiltered(title, year));
        return mv;
    }
}
