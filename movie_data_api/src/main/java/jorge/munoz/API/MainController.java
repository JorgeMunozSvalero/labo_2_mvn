package jorge.munoz.API;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
   
    @GetMapping("")
    public String getIndex(String lang){
        return "home";
    }
    
    @GetMapping("/starter")
    public String getStart(String lang){
        return "home";
    }

    @GetMapping("/search")
    public String getSearch(String lang){
        return "search";
    }

    @GetMapping("/detail")
    public String getDetail(String lang){
        return "detail";
    }

}
